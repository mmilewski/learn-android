package com.marcin;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class FirstActivity extends Activity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        findViewById(R.id.button).setOnClickListener(this);
    }

    @Override
    public void onClick(View src) {
        switch (src.getId()) {
            case R.id.button: {
                EditText editText = (EditText) findViewById(R.id.editText);
                TextView tv = (TextView) findViewById(R.id.tv);
                tv.setText(String.valueOf(editText.getText()));

                break;
            }
        }

    }
}
